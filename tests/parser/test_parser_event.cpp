/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developpers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Clément 
**
*/
#include "../../src/parser/Line.hpp"
#include "../../src/parser/ParserDefinitionDecoder.hpp"
#include "../../src/parser/ParserEventDecoder.hpp"
#include "../stubs/Trace.hpp"
#include <iostream>
#include <fstream>
#include <cstdlib>

#define DIE_IF(condition,message) if (condition){       \
        cout << message << endl;                        \
        return 0;}


using namespace std;
void stop(){}
int main(int argc, char** argv){

    ParserDefinitionDecoder *parserdefinition = new ParserDefinitionDecoder();
    ParserEventDecoder      *parserevent = new ParserEventDecoder();
    Trace trace;
  
    Line line;
    if (argc<2)
        line.open("trace_to_parse.trace");
    else
        line.open(argv[1]);

    int linecount = 0;
    static const string percent = "%";
    string event_identity_string;
    int event_identity;
    
    while(!line.is_eof()){

	line.newline();

	if(line.starts_with(percent)){
	    parserdefinition->store_definition(line);
            linecount ++;
	}
	else if (!line.item(0, event_identity_string)){
            continue;
        }
	else{
            stop();
            
	    DIE_IF(sscanf(event_identity_string.c_str(), "%d", &event_identity) != 1, "expected identifier for a definition");

	    parserevent->store_event(
                                     parserdefinition->get_definition(event_identity),
                                     line,
                                     trace);

            ;
	}
    }

    //parserdefinition->print_definitions();
    cout << "lu :" << linecount << endl;

    delete parserdefinition;
    delete parserevent;
    return EXIT_SUCCESS;
}

#ifndef _VITE_GENERATORWINDOW_
#define _VITE_GENERATORWINDOW_

#include <QMainWindow>
#include <QtGui/QApplication>

#include "ui_mainWindowGenerate.h"
#include "Generator.hpp"


    // Default values
static const QString NAME          = "trace";
static const int     DEPTH         = 2;
static const int     PROCNBR       = 16;
static const int     NUMBEROFSTATE = 10;
static const int     NUMBEROFLINE  = 10000;
static const bool    COUNTER       = true;
static const bool    EVENT         = true;
static const bool    LINK          = true;
static const int     LINKNBR       = 8;
static const int     EVENTNBR      = 8;
static const int     COUNTERNBR    = 8;
static const int     LINKTYPENBR   = 5;
static const int     EVENTTYPENBR  = 5;


class GeneratorWindow : public QMainWindow, protected Ui_generatorBody{
    Q_OBJECT
private :
    QApplication* _app;

    // Values 
    QString _name          ; // Name of the file to create
    int     _depth         ; // Depth of the container tree
    int     _procNbr       ; // Number of procs
    int     _numberOfState ; // Number of different states
    int     _numberOfLine  ; // Number of lines in the file
    bool    _counter       ; // If counter are enabled
    bool    _event         ; // If events are enabled
    bool    _link          ; // If link are enabled
    int     _linkNbr       ; // Number of proc with links
    int     _eventNbr      ; // Number of proc with events
    int     _linkTypeNbr   ; // Number of type of links
    int     _eventTypeNbr  ; // Number of type of events
    int     _counterNbr    ; // Number of proc with counters
    bool    _paje          ; // If paje enabled
    bool    _tau           ; // If tau enabled
    bool    _otf           ; // If otf enabled

    // Trace Writers
    Generator* _gene;


    // UI Elements
    QLineEdit* _ui_name         ;
    QLineEdit* _ui_depth        ;
    QLineEdit* _ui_procNbr      ;
    QLineEdit* _ui_numberOfState;
    QLineEdit* _ui_numberOfLine ;
    QLineEdit* _ui_counterNbr   ;
    QLineEdit* _ui_linkNbr      ;
    QLineEdit* _ui_eventNbr     ;
    QLineEdit* _ui_linkTypeNbr  ;
    QLineEdit* _ui_eventTypeNbr ;
    QCheckBox* _ui_counter      ;
    QCheckBox* _ui_link         ;
    QCheckBox* _ui_event        ;
    QLabel   * _ui_counterLabel ;
    QLabel   * _ui_linkLabel    ;
    QLabel   * _ui_eventLabel   ;
    QCheckBox* _ui_paje         ;
    QCheckBox* _ui_tau          ;
    QCheckBox* _ui_otf          ;
    QPushButton* generateButton;
    QPushButton* resetButton;

    // To hide the counter edit line
    void hideCounter ();
    // To hide the link edit line
    void hideLink    ();
    // To hide the event edit line
    void hideEvent   ();
    // To show the counter edit line
    void showCounter ();
    // To show the link edit line
    void showLink    ();
    // To show the event edit line
    void showEvent   ();
    // To set the param to launch
    void setParam    ();
    // To end the creation
    void finishGeneration ();

public :

    // Constructor
    GeneratorWindow  (QWidget* parent, QApplication* app);
    // Destructor
    ~GeneratorWindow ();
    // Main function that launch the creation of the traces
    void generate    ();
    void cleanParam ();
    int run ();

protected slots :
    void generateButton_clicked ();
    void resetButton_clicked    ();



};


#endif


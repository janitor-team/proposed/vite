#include "OTFGenerator.hpp"

OTFGenerator::OTFGenerator   (){
}

OTFGenerator::~OTFGenerator  (){
}

void OTFGenerator::initTrace (QString name, int depth, int procNbr, int stateNbr, int eventNbr, int linkTypeNbr, int varNbr){
}

void OTFGenerator::addState  (int proc, int state, double time){
}

void OTFGenerator::startLink (int proc, int type , double time){
}

void OTFGenerator::endLink   (int proc, int type , double time){
}

void OTFGenerator::addEvent  (int proc, int type , double time){
}

void OTFGenerator::incCpt    (int proc, int var  , double time){
}

void OTFGenerator::decCpt    (int proc, int var  , double time){
}

void OTFGenerator::endTrace  () {
}



#include "Plugin.hpp"

class Plug2 : public Plugin {
public: 
    void init() { printf("Plug2 : init\n"); }
    void set_arguments(std::map<std::string /*argname*/, QVariant */*argValue*/>) {};

    std::string get_name() { return "Plug2"; }

public slots:
    void execute() { printf("Plug2 : exec\n"); }
};

extern "C" {
    Plugin *create() { return new Plug2(); }
}

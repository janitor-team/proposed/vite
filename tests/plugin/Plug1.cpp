#include "Plugin.hpp"

class Plug1 : public Plugin {
public: 
    void init() { printf("Plug1 : init\n"); }
    void set_arguments(std::map<std::string /*argname*/, QVariant */*argValue*/>) {}
    std::string get_name() { return "Plug1"; }

public slots:
    void execute() { printf("Plug1 : exec\n"); }
};

extern "C" {
    Plugin *create() { return new Plug1(); }
}

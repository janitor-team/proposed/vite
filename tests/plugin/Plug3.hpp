#ifndef PLUG3_HPP
#define PLUG3_HPP

#include "Plugin.hpp"

class Plug3 : public Plugin {
public: 
    void init() { printf("Plug3 : init\n"); }
    void set_arguments(std::map<std::string /*argname*/, QVariant */*argValue*/>) {};

    std::string get_name() { return "Plug3"; }

public slots:
    void execute() { printf("Plug3 : exec\n"); }
};

#endif // PLUG3_HPP

#include <QApplication>
#include "Plugin.hpp"
#include "PluginWindow.hpp"


int main(int argc, char **argv) {
    
    QApplication app(argc, argv);

    PluginWindow *wind = new PluginWindow();
    
    wind->load_plugin("plug3");

    wind->show();
    return app.exec();
}

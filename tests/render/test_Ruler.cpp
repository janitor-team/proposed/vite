#include <iostream>
typedef double Element_pos;
#include "../../src/render/Ruler.hpp"


void display_information(const Element_pos min, const Element_pos max){
  Element_pos graduation_diff, coeff_prefix;
  Element_pos min_common_part, min_variable_part;
  Element_pos max_common_part, max_variable_part;


  graduation_diff = Ruler::get_graduation_diff(min, max);
  coeff_prefix    = Ruler::get_coeff_for_common_prefix(min, max);
        
  min_common_part   = Ruler::get_common_part(min, coeff_prefix);
  min_variable_part = Ruler::get_variable_part(min, coeff_prefix, 2);

  max_common_part   = Ruler::get_common_part(max, coeff_prefix);
  max_variable_part = Ruler::get_variable_part(max, coeff_prefix, 2);
             
  std::cout << "Display result for min=" << min << " and max=" << max << "\n"
            << "Graduation difference=" << graduation_diff << "\n"
            << "Coefficient for common prefix=" << coeff_prefix << "\n"
            << "Minimum:\n\tCommon part=" << min_common_part << "\n"
            << "\tVariable part=" << min_variable_part << "\n"
            << "Maximum:\n\tCommon part=" << max_common_part << "\n"
            << "\tVariable part=" << max_variable_part << "\n" << std::endl;
}


int main(int argc, char** argv){
  Element_pos min, max;

  display_information(1.0, 2.0);
  display_information(0.0, 0.50);
  display_information(0.10, 0.104);
  display_information(0.9, 1.0);

  return 0;
}

#ifndef MATRIX_WINDOW_HPP
#define MATRIX_WINDOW_HPP

#include "../Formats/SymbolMatrix.hpp"
#include "MatrixGLWidget.hpp"

#include <QMainWindow>
#include <QCloseEvent>
#include <QKeyEvent>
#include <QLabel>

#define MATRIX_WINDOW_LENGTH 512
#define MATRIX_WINDOW_HEIGHT 512

class Matrix_window : public QMainWindow
{
public:
    Matrix_window(symbol_matrix_t* matrix);
    ~Matrix_window();

protected:
    void closeEvent(QCloseEvent* event);
    void keyPressEvent(QKeyEvent* keyEvent);

private:
    MatrixGLWidget* m_gl;
    QLabel*         m_label;
};

#endif

#include "MatrixWindow.hpp"

#include <QVBoxLayout>

Matrix_window::Matrix_window(symbol_matrix_t* matrix)
{
    QWidget* widget = new QWidget();
    QVBoxLayout* layout = new QVBoxLayout(widget);

    m_label = new QLabel(nullptr);
    m_label->setText("Infos: ");

    m_gl = new MatrixGLWidget(nullptr, matrix, m_label);
    m_gl->setObjectName(QString("matrix_gl_visualizer"));

    layout->addWidget(m_gl);
    layout->addWidget(m_label);

    this->setCentralWidget(widget);
}

Matrix_window::~Matrix_window()
{
    delete m_gl;
}

void Matrix_window::closeEvent(QCloseEvent* event)
{
    delete this;
}

void Matrix_window::keyPressEvent(QKeyEvent* event)
{
    switch (event->key())
    {
    case Qt::Key_Escape:
        close();
        break;
    }
}

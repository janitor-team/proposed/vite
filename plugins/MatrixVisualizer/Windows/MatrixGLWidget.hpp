#ifndef MATRIX_GL_WIDGET_HPP
#define MATRIX_GL_WIDGET_HPP

#include "Configuration.hpp"

#include <QtOpenGL/QtOpenGL>

#ifdef USE_QT5
#include <QOpenGLFunctions>
#include <QOpenGLWidget>
#else
#include <QGLWidget>
#endif
#include <QKeyEvent>

#include <stack>

#include "../Formats/SymbolMatrix.hpp"
#include "../Common/Zooming.hpp"

class MatrixGLWidget :
#ifdef USE_QT5
    public QOpenGLWidget, protected QOpenGLFunctions
#else
    public QGLWidget
#endif
{
public:
    MatrixGLWidget(QWidget* parent, symbol_matrix_t* matrix, QLabel* label);
    ~MatrixGLWidget();

protected:
    void initializeGL() override;
    void resizeGL(int w, int h) override;
    void paintGL() override;

    void keyPressEvent(QKeyEvent *keyEvent);

    void mousePressEvent(QMouseEvent* mouseEvent);
    void mouseReleaseEvent(QMouseEvent* mouseEvent);
    void mouseMoveEvent(QMouseEvent* mouseEvent);

private:
    typedef struct CameraPosition
    {
        double        m_cameraX;
        double        m_cameraY;
        double        m_cameraDx;
        double        m_cameraDy;
    } CameraPosition;

private:
    void refreshCamera();

private:
    uint32_t        m_frameCount;
    QTime           m_time;
    QLabel*         m_label;

    char            m_fpsString[256] = { '\0' };

    double          m_qtToGLWidthCoeff;
    double          m_qtToGLHeightCoeff;

    // Zoom
    Zooming*        m_zooming;
    int             m_mouseXClicked;
    int             m_mouseYClicked;
    CameraPosition  m_camera;

    int             m_drawTempSelection = 0;
    int             m_tempSelectionX    = 0;
    int             m_tempSelectionY    = 0;
    int             m_tempSelectionDx   = 0;
    int             m_tempSelectionDy   = 0;

    // Zoom stack
    std::stack<CameraPosition> m_savedPositions;
};

#endif

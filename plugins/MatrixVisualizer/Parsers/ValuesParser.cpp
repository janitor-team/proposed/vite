#include "ValuesParser.hpp"
#include "Readers/Pastix.hpp"

ValuesParser::ValuesParser()
{
    m_percentage_loaded = 0;
}

symbol_matrix_t* ValuesParser::parse(std::string path, symbol_matrix_t *matrix)
{
    return matrix;
}

float ValuesParser::get_percent_loaded() const
{
    return m_percentage_loaded;
}

bool ValuesParser::is_finished() const
{
    return m_is_finished;
}

bool ValuesParser::is_cancelled() const
{
    return m_is_canceled;
}

void ValuesParser::set_canceled()
{
    m_is_canceled = true;
}

void ValuesParser::finish()
{
    m_is_finished = true;
}

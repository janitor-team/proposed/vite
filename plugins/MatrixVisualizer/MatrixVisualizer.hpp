#ifndef MATRIX_VISUALIZER_HPP
#define MATRIX_VISUALIZER_HPP

#include "Windows/MatrixWindow.hpp"
#include "Parsers/SymbolParser.hpp"
#include "Parsers/OrderParser.hpp"
#include "Parsers/ValuesParser.hpp"
#include "plugin/Plugin.hpp"
#include "Helper.hpp"

// Generated
#include "ui_Plugin.h"

class Matrix_visualizer : public Plugin, public Ui::Matrix_visualizer_plugin_ui {
    Q_OBJECT
public:
    static Matrix_visualizer* get_instance();

    void init();
    void clear();

    void set_arguments(std::map<std::string /*argname*/, QVariant* /*argValue*/>);
    std::string get_name();

    void log(LogStatus status, const char* format, va_list ap);
    void set_infos(const char* format, va_list ap);

private:
    Matrix_visualizer();
    ~Matrix_visualizer();

    int update_matrix( QString filepath );
    int update_order ( QString filepath );
    int update_values( QString filepath );

    void connect_widgets();
    void set_line_edit_defaults( QLineEdit *le, std::string name, std::string def_name );

public slots:
    void execute();

private slots:
    static void on_tool_button_symbol_clicked();
    static void on_tool_button_order_clicked();
    static void on_tool_button_values_clicked();
    static void on_tool_button_infos_clicked();
    static void on_tool_button_logs_clicked();

private:
    static Matrix_visualizer*   s_plugin;

    // Parsers
    static SymbolParser         s_symbol_parser;
    static OrderParser          s_order_parser;
    static ValuesParser         s_values_parser;

    // Windows
    static Matrix_window*       s_matrix_window;

    // Matrix
    static symbol_matrix_t*     s_matrix;
};

extern "C"
#ifdef WIN32
    __declspec(dllexport) // no comment
#endif
Plugin *create();

#endif

#ifndef QUADTREE_HPP
#define QUADTREE_HPP

#include <QtOpenGL/QtOpenGL>

#include "../Formats/SymbolMatrix.hpp"

class Zooming
{
public:
    static const int DEFAULT_LEVEL = 10;
    static const int DEFAULT_LEVEL_POWER_2 = 1024;

public:
    Zooming(symbol_matrix_t* matrix);
    ~Zooming();

    GLfloat getColor(int x, int y);

    void move(double xStart, double xEnd, double yStart, double yEnd);

private:
    symbol_matrix_t *m_matrix;
    GLfloat          m_colors[DEFAULT_LEVEL_POWER_2][DEFAULT_LEVEL_POWER_2];
};

#endif

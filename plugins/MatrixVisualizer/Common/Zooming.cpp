#include <assert.h>
#include <math.h>
#include "Zooming.hpp"

#include "../Helper.hpp"

Zooming::Zooming(symbol_matrix_t* matrix)
    : m_matrix(matrix)
{
    // Fill correct colors
    move(0.f, 1.f, 0.f, 1.f);
}

Zooming::~Zooming()
{
}

GLfloat Zooming::getColor(int x, int y)
{
    return m_colors[x][y];
}

void Zooming::move(double xStart, double xEnd, double yStart, double yEnd)
{
    symbol_cblk_t* cblk;
    symbol_blok_t* blok;
    int i, j, m, n;

    // Check positions
    assert( xEnd >= xStart );
    assert( yEnd >= yStart );
    assert( yEnd >= xStart );

    // Check for out of bounds
    assert( xStart >= 0. );
    assert( yStart >= 0. );
    assert( xEnd <= 1. );
    assert( yEnd <= 1. );

    // Convert to column/row indexes
    int startCol = xStart * m_matrix->m_colsnbr;
    int endCol   = xEnd   * m_matrix->m_colsnbr - 1;
    int startRow = yStart * m_matrix->m_rowsnbr;
    int endRow   = yEnd   * m_matrix->m_rowsnbr - 1;

    // Find first cblk
    int startCblk = 0;
    int endCblk = 0;

    // Search for beginnings
    cblk = m_matrix->m_cblktab;
    for (i = 0; i < m_matrix->m_cblknbr; ++i, cblk++)
    {
        if ( (cblk->m_fcolnum <= startCol) && (startCol <= cblk->m_lcolnum) )
        {
            startCblk = i;
            startCol = cblk->m_fcolnum;
        }
        if ( (cblk->m_fcolnum <= startRow) && (startRow <= cblk->m_lcolnum) )
        {
            startRow = cblk->m_fcolnum;
        }
        if ( (cblk->m_fcolnum <= endCol) && (endCol <= cblk->m_lcolnum) )
        {
            endCblk = i+1;
            endCol = cblk->m_lcolnum;
        }
        if ( (cblk->m_fcolnum <= endRow) && (endRow <= cblk->m_lcolnum) )
        {
            endRow = cblk->m_lcolnum;
        }
    }

    int nb_cols = endCol - startCol + 1;
    int nb_rows = endRow - startRow + 1;

    for (i = 0; i < DEFAULT_LEVEL_POWER_2; ++i)
    {
        for (j = 0; j < DEFAULT_LEVEL_POWER_2; ++j)
        {
            m_colors[i][j] = 1.f;
        }
    }

    float xCoeff = (float)DEFAULT_LEVEL_POWER_2 / ((float)nb_cols);
    float yCoeff = (float)DEFAULT_LEVEL_POWER_2 / ((float)nb_rows);

    cblk = m_matrix->m_cblktab + startCblk;
    for (i = startCblk; i < endCblk; ++i, cblk++)
    {
        int fbloknum = cblk[0].m_bloknum;
        int lbloknum = cblk[1].m_bloknum;

        // Get first block size in col from x to xEnd
        int x    = (cblk->m_fcolnum     - startCol) * xCoeff;
        int xEnd = (cblk->m_lcolnum + 1 - startCol) * xCoeff;

        float cblk_color = cblk->m_color;

        blok = m_matrix->m_bloktab + fbloknum;
        for (j = fbloknum; j < lbloknum; ++j, blok++)
        {
            if ( (blok->m_lrownum < startRow) ||
                 (blok->m_frownum > endRow  ) )
            {
                continue;
            }

            // Get first block size in row from y to yEnd
            int y    = (blok->m_frownum     - startRow) * yCoeff;
            int yEnd = (blok->m_lrownum + 1 - startRow) * yCoeff;

            float color = blok->m_color == -1. ? cblk_color : blok->m_color;

            for( m=x; m<xEnd; m++ )
            {
                for( n=y; n<yEnd; n++ )
                {
                    m_colors[m][n] = color;
                }
            }
        }
    }
}

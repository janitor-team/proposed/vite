#ifndef DEF_SHADER
#define DEF_SHADER
//#include <GL/glew.h>


#include "common/common.hpp"
// Includes communs

#include <iostream>
#include <string>
#include <fstream>
// Classe Shader

class Shader
{
public:
    /* Default constructor*/
    Shader();

    /*Constructor for shaders using textures. Parameter n is not used, it just allow to create a have another constructor. */
    Shader(int glsl, int n);

    /* Construct a Shader reading colors in the VBO. Still used for containers and selection*/
    Shader(int glsl);

    /* Construct Shaders we use for entities
     param glsl says which version of GL Shading Language we are using
     param rgb define the color
     param shade should be true for states, false either*/
    Shader(int glsl, Element_col, Element_col, Element_col, bool);

    //Destructor
    ~Shader();

    Shader& operator=(Shader const &shaderACopier);

    /*Loading function*/
    bool charger();
    /*Compilation function*/
    bool compilerShader(GLuint &shader, GLenum type, const std::string &code);
    /*Getter*/
    GLuint getProgramID() const;

private:

    GLuint m_vertexID {0};
    GLuint m_fragmentID {0};
    GLuint m_programID {0};

    std::string m_vertex_code;
    std::string m_fragment_code;
};

#endif

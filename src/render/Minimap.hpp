/*
 ** This file is part of the ViTE project.
 **
 ** This software is governed by the CeCILL-A license under French law
 ** and abiding by the rules of distribution of free software. You can
 ** use, modify and/or redistribute the software under the terms of the
 ** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
 ** URL: "http://www.cecill.info".
 **
 ** As a counterpart to the access to the source code and rights to copy,
 ** modify and redistribute granted by the license, users are provided
 ** only with a limited warranty and the software's author, the holder of
 ** the economic rights, and the successive licensors have only limited
 ** liability.
 **
 ** In this respect, the user's attention is drawn to the risks associated
 ** with loading, using, modifying and/or developing or reproducing the
 ** software by the user in light of its specific status of free software,
 ** that may mean that it is complicated to manipulate, and that also
 ** therefore means that it is reserved for developers and experienced
 ** professionals having in-depth computer knowledge. Users are therefore
 ** encouraged to load and test the software's suitability as regards
 ** their requirements in conditions enabling the security of their
 ** systems and/or data to be ensured and, more generally, to use and
 ** operate it in the same conditions as regards security.
 **
 ** The fact that you are presently reading this means that you have had
 ** knowledge of the CeCILL-A license and that you accept its terms.
 **
 **
 ** ViTE developers are (for version 0.* to 1.0):
 **
 **        - COULOMB Kevin
 **        - FAVERGE Mathieu
 **        - JAZEIX Johnny
 **        - LAGRASSE Olivier
 **        - MARCOUEILLE Jule
 **        - NOISETTE Pascal
 **        - REDONDY Arthur
 **        - VUCHENER Clément
 **
 */
/*!
 *\file Minimap.hpp
 */

#ifndef MINIMAP_HPP
#define MINIMAP_HPP

class QLabel;
class QImage;
class QPainter;
class QPen;
class QCloseEvent;


/*!
 * \brief This class redefined the QLabel widget to display a minimap.
 */
class Minimap: public QLabel
{
protected:

    /*!
     * \brief Viewport square coordinates and dimensions.
     */
    int _x, _y, _w, _h;

    /*!
     * \brief QImage to store the trace thumbnail.
     */
    QImage _minimap_image;

    /*!
     * \brief Store the original image before being scaled for _minimap_image.
     */
    QImage _original_image;

    /*!
     * \brief Painter used to draw the minimap image and current view square.
     */
    QPainter _painter;

    /*!
     * \brief Pen used with the painter.
     */
    QPen _pen;

    /*!
     * \brief Boolean used to know if the minimap needs to be initialized.
     */
    bool _is_initialized;


    /*!
     * \brief Called by Qt when the user manually closes the window.
     * \param event The Qt event.
     */
    void closeEvent(QCloseEvent *event) override;

    /*!
     * \brief Called by Qt when the user manually resize the window.
     * \param event The Qt event.
     */
    void resizeEvent(QResizeEvent* event) override;


    /*!
     * \brief Redraw the minimap according to the _minimap_image, _x, _y, _w and _h values.
     */
    void redraw();

public:

    /***********************************
     *
     * Constructor and destructor.
     *
     **********************************/

    /*!
     * \brief The default constructor
     */
    Minimap();

    /*!
     * \brief Init the minimap image.
     * \param image QImage used for the minimap image.
     */
    void init(const QImage &image);

    /*!
     * \brief Check if the minimap has been initialized
     */
    bool is_initialized();

    /*!
     * \brief Update the minimap according to the current viewport (viewport is define by x, y, w and h).
     */
    void update(const int x, const int y, const int w, const int h);

    /*!
     * \brief Release the minimap data.
     */
    void release();
};

#endif

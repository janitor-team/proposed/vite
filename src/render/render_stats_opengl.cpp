/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Clément
**
*/
/*!
 *\file render_stats_opengl.cpp
 */

#ifdef _WIN32
        #include <Windows.h>
#endif

#include <cstdio>
#include <string>
/* -- */
#include <GL/glu.h>
/* -- */
#include "common/common.hpp"
#include "common/Info.hpp"
/* -- */
#include "interface/resource.hpp"
#include "interface/Interface.hpp"
#include "core/Core.hpp"
/* -- */
#include "render/render_stats_opengl.hpp"
/* -- */
using namespace std;

Render_stats_opengl::Render_stats_opengl(QWidget *parent) : QGLWidget(parent), _stats_beginned_drawed(false) {
    _translated_y  = 0.;
    _translated_x  = 0.;
    _render_height = height();
    _render_width  = width();
    _rect_list = glGenLists(1);

}

Render_stats_opengl::~Render_stats_opengl(){
    _text_pos.clear();
    _text_value.clear();
    glDeleteLists(_rect_list, 1);
}

void Render_stats_opengl::initializeGL(){
    glEnable(GL_DEPTH_TEST);
}


void Render_stats_opengl::resizeGL(int width, int height){
    glViewport(0, 0, width, height);

    /* update informations about widget size */
    _screen_width = width;
    _screen_height = height;

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glMatrixMode(GL_MODELVIEW);

}

void Render_stats_opengl::paintGL(){

    makeCurrent();
    glClearDepth(1.0);
    glClearColor(1.f, 1.f, 1.f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glLoadIdentity();

    glMatrixMode(GL_PROJECTION);
    gluOrtho2D(0, _screen_width, 0, _screen_height);

    glTranslated(-_translated_x, _translated_y, 0);


    if(!_stats_beginned_drawed) {
        return;
    }

    list<Element_pos>::const_iterator it_pos;
    list<string>::const_iterator it_txt;
    Element_pos buf_x;
    Element_pos buf_y;
    string buf_txt;

    if (glIsList(_rect_list) == GL_FALSE)
        printf("ERROR LIST not exist for stats\n");
    else
        glCallList(_rect_list);

    glFlush();

    /* Check the errors */
    GLenum glerror;
    glerror = glGetError();
    if(glerror != GL_NO_ERROR) {
        fprintf(stderr, "Render stats openGL : the following OpengGL error occured: %s\n", gluErrorString(glerror));
    }

    const QFont &arial_font = QFont("Arial", 10);
    qglColor(QColor(0, 0, 0)); // We put the text in black
    for (it_txt=_text_value.begin(), it_pos=_text_pos.begin() ; it_txt!=_text_value.end() ; it_txt ++, it_pos ++) {

        buf_x = *it_pos - _translated_x;
        it_pos ++;

        buf_y =  *it_pos - _translated_y + height() - _size_for_one_container;

        buf_txt = *it_txt;

        //painter.drawText(buf_x, buf_y, buf_txt.c_str());
        renderText(buf_x, buf_y, buf_txt.c_str(), arial_font);
    }

//     painter.end();
}

void Render_stats_opengl::translate_y(int value) {
    if(value == 0) {
        _translated_y = 0;
    }
    else {
        _translated_y = (double)(value+1)/100.0*_render_height;
    }
    updateGL();
}

void Render_stats_opengl::translate_x(int value) {
    if(value == 0) {
        _translated_x = 0;
    }
    else {
        _translated_x = (double)(value+1)/100.0*_render_width;
    }
    //cout << "w :" << _render_width << endl;
    updateGL();
}

void Render_stats_opengl::set_total_height(Element_pos h) {
    _render_height = h;
}

void Render_stats_opengl::set_height_for_one_container(Element_pos h) {
    _size_for_one_container = h;
}

void Render_stats_opengl::set_total_width(Element_pos w) {
    _render_width = w;
}

void Render_stats_opengl::clear(){
    makeCurrent();
    _translated_y = 0;
    _translated_x = 0;
    _text_pos.clear();
    _text_value.clear();
}

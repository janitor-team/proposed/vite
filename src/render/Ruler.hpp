/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Clément 
**
*/
/*!
 *\file Ruler.hpp
 */

#ifndef RULER_HPP
#define RULER_HPP

/*!
 * \class Ruler
 * \brief Represents the displayed time ruler on the graphical interface.
 */
class Ruler {
    
protected:


private:

    /*!
     * \brief The default constructor. In private scope to prevent instance.
     */
    Ruler() = default;

    /***********************************
     *
     * Ruler methods.
     *
     **********************************/
public:

    /*!
     * \brief Return the distance between two graduation in trace coordinates
     * \arg min The minimum time visible. (In trace coordinate.)
     * \arg max The maximum time visible. (In trace coordinate.)
     * \return Most suitable distance between two consecutive graduations within [min ; max].
     */
    static Element_pos get_graduation_diff(const Element_pos min, const Element_pos max);

    /*!
     * \brief Work out the coefficient (or the comma shift) to separate the common part 
     *        (at the left of the comma) from the variable part. (at the right of the comma)
     * \arg min The minimum time visible. (In trace coordinate.)
     * \arg max The maximum time visible. (In trace coordinate.)
     * \return The coefficient used to separate the common and the variable part of graduation numbers.
     */
    static Element_pos get_coeff_for_common_prefix(Element_pos min, Element_pos max);


    /*!
     * \brief Return the common part of n within [min ; max].
     * \arg n A time. (In trace coordinate.)
     * \arg coeff_for_common_prefix Coefficient returned by the Ruler::get_coeff_for_common_prefix() method
     * \return A float. The common part of n according to the common prefix coefficient got from the 
     *         Ruler::get_coeff_for_common_prefix() method.
    */
    static double get_common_part(const Element_pos n, Element_pos coeff_for_common_prefix);


    /*!
     * \brief Return a string containing the common part of n within [min ; max]. Variable part is represented with '-'.
     * \arg n A time. (In trace coordinate.)
     * \arg coeff_for_common_prefix Coefficient returned by the Ruler::get_coeff_for_common_prefix() method
     * \return A reference to a string.
    */
    static std::string get_common_part_string(const Element_pos n, const Element_pos coeff_for_common_prefix);

    /*!
     * \brief Return the variable part of n within [min ; max].
     * \arg n A time. (In trace coordinate.)
     * \arg coeff_for_common_prefix Coefficient returned by the Ruler::get_coeff_for_common_prefix() method
     * \return A float. The variable part of n according to the common prefix coefficient got from the 
     *         Ruler::get_coeff_for_common_prefix() method.
    */
    static double get_variable_part(const Element_pos n, const Element_pos coeff_for_common_prefix, const int nb_digit_after_comma);
};



#endif

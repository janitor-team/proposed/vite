/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Clément 
**
*/
/*!
 *\file render_stats_svg.cpp
 */

/* Global informations */
#include <fstream>
#include <string>
#include <list>
/* -- */
#include "common/common.hpp"
/* -- */
#include "render/render_stats.hpp"
#include "render/render_stats_svg.hpp"
/* -- */
using namespace std;

Render_stats_svg::Render_stats_svg(string filename){
    _filename = std::move(filename);
    _y_min = 0;
    _y_max = 0;
}

Render_stats_svg::~Render_stats_svg() = default;


void Render_stats_svg::set_total_height(Element_pos h) {
    _render_height = h;
}


void Render_stats_svg::set_total_width(Element_pos w) {
    _render_width = w;
}

void Render_stats_svg::clear(){
}

void Render_stats_svg::fill_file(){
    list<Element_pos>::const_iterator it_pos;
    list<string>::const_iterator      it_txt;
    list<Element_pos>::const_iterator it_line;
    list<Element_pos>::const_iterator it_rect;
    Element_pos buf_x;
    Element_pos buf_y;
    string      buf_txt; 
    Element_pos height = _y_max - _y_min + 20;

    /* Draw rect */
    for (it_rect = _rect_pos.begin() ; it_rect!=_rect_pos.end() ; it_rect ++) {
        const Element_pos &x = *it_rect;
        it_rect ++;
        const Element_pos &y = *it_rect;
        it_rect ++;
        const Element_pos &w = *it_rect;
        it_rect ++;
        const Element_pos &h = *it_rect;
        it_rect ++;
        const Element_pos &r = *it_rect;
        it_rect ++;
        const Element_pos &g = *it_rect;
        it_rect ++;
        const Element_pos &b = *it_rect;

        _svg_file << "<rect width=\"" << w
                  << "\" height=\""   << h
                  << "\" x=\""        << x
                  << "\" y=\""        << height-(y-_y_min)-h
                  << "\" fill=\"rgb(" << (int)(r*255)<<","<<(int)(g*255)<<","<<(int)(b*255)
                  << ")\"/>\n";
        
    }/* end for(...) */
  
    /* Draw lines */
    for (it_line=_line_pos.begin() ; it_line!=_line_pos.end() ; it_line ++) {
        const Element_pos &x = *it_line;
        it_line ++;
        const Element_pos &y = *it_line;
        it_line ++;
        const Element_pos &size_x = *it_line;
        it_line ++;
        const Element_pos &size_y = *it_line;
        
        _svg_file << "<line x1=\"" << x 
		  << "\" y1=\""    << height-(y-_y_min)
		  << "\" x2=\""    << x+size_x 
		  << "\" y2=\""    << height-(y-_y_min+size_y)
		  << "\" stroke=\"black\" />\n";
    }/* end for(...) */

    /* Draw texts */
    for (it_txt=_text_value.begin(), it_pos=_text_pos.begin() ; it_txt!=_text_value.end() ; it_txt ++, it_pos ++) 
      { 
        buf_x   = *it_pos; it_pos ++;
	buf_y   = *it_pos;
        buf_txt = *it_txt;
            
        _svg_file << "<text x=\"" << buf_x 
		  <<"\" y=\""     << buf_y-_y_min+20
		  << "\">"        << buf_txt
		  << "</text>\n";
      }/* end for(...) */
}

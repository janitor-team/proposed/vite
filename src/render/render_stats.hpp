/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Clément 
**
*/
/*!
 *\file render_stats.hpp
 */

#ifndef RENDER_STATS_HPP
#define RENDER_STATS_HPP


class Render_stats;

/*!
 * \brief This class provides an interface for render_stats classes like OpenGL or SVG.
 */
class Render_stats { 

protected:
        /*!
     * \brief The opengl render area width in pixels.
     */
    Element_pos _screen_width;

    /*!
     * \brief The opengl render area height in pixels.
     */
    Element_pos _screen_height;

    /*!
     * \brief The opengl visibled scene width in the OpenGL units.
     */
    Element_pos _render_width;

    /*!
     * \brief The opengl visibled scene height in the OpenGL units.
     */
    Element_pos _render_height;

    /*!
     * \brief The height used to show one container (fixed for now)
     */
    Element_pos _size_for_one_container;

public:
    /*!
     * \brief Proceeds with the initialization of draw functions.
     */
    virtual void start_draw() = 0;

    /*!
     * \brief Draw the text.
     * \param x the x position of the text.
     * \param y the y position of the text.
     * \param value the string value of the text.
     */
    virtual void draw_text(const Element_pos x, const Element_pos y, const std::string value) = 0;

    /*!
     * \brief Proceeds with the end of draw functions.
     */
    virtual void end_draw() = 0;

    /*!
     * \brief Set the height of the render area.
     */
    virtual void set_total_height(Element_pos h) = 0;

    /*!
     * \brief Set the height for one container.
     */
    virtual void set_height_for_one_container(Element_pos h) = 0;

};



#endif

/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Clément
**
*/
/*!
 * \mainpage ViTE
 *
 ViTE is a trace explorer. It is a tool to visualize execution traces in Pajé format for debugging and profiling parallel or distributed applications.
 * \image html logo.png
 * \image latex logo.png
 *
 * \authors COULOMB Kevin
 * \authors FAVERGE Mathieu
 * \authors JAZEIX Johnny
 * \authors LAGRASSE Olivier
 * \authors MARCOUEILLE Jule
 * \authors NOISETTE Pascal
 * \authors REDONDY Arthur
 * \authors VUCHENER Clément
 *
 * \file main.cpp
 * \brief The main launcher.
 */


#include <QObject>
/* -- */
#include <cstdio>
#include <cstdlib>
#include <string>
#include <stack>
/* -- */
/* Global informations */
#include "common/common.hpp"
#ifdef MEMORY_USAGE
#include "common/Memory.hpp"
#endif
/* -- */
#include "common/Info.hpp"
#include "interface/Interface.hpp"
#include "core/Core.hpp"


/*!
 *\brief The main function of ViTE.
 */
int main(int argc, char **argv) {

#ifdef VITE_DBG_MEMORY_TRACE
    FILE * file;
    double timestamp;
#endif

    Q_INIT_RESOURCE(vite);

    Info::Render::_key_alt = false;/* no CTRL key pushed */

#ifdef VITE_DBG_MEMORY_TRACE
    timestamp = clockGet();
    file      = fopen("toto.trace", "w");
    memAllocTrace(file, timestamp, 0);
    trace_start(file, timestamp, 0, -1);
#endif /* VITE_DBG_MEMORY_TRACE */

    Core console(argc, argv);
    console.run();

#ifdef MEMORY_USAGE
#ifdef VITE_DBG_MEMORY_TRACE
    trace_finish(file, (clockGet()-timestamp), 0, -1);
    memAllocUntrace();
#endif
    fprintf(stdout, "Max Memory allocated   : %lu\n", memAllocGetMax());
    fprintf(stdout, "Memory still allocated : %lu\n", memAllocGetCurrent());
#endif

    return EXIT_SUCCESS;
}

/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Clément 
**
*/
#ifndef PARSING_THREAD_HPP
#define PARSING_THREAD_HPP

#include <QWaitCondition>
#include <QMutex>
#include <QObject>

class Trace;
class Parser;

/*!
 * \class ParsingThread
 * \brief Thread to parse asynchronously a trace with any parser available
 */
class ParsingThread : public QObject {
    Q_OBJECT

private:
    Parser         *_parser;
    Trace          *_trace;
    QWaitCondition *_finished;
    QWaitCondition *_closed;
    QMutex         *_mutex;
    bool            _is_finished;
    bool            _is_killed;
    
public:
    /*!
     *  \fn ParsingThread(Parser *p, Trace *t)
     *  \param p the parser used to parse the file.
     *  \param t the trace where we store data.
     */
    ParsingThread(Parser         *p, 
                  Trace          *t,
                  QWaitCondition *finished, 
                  QWaitCondition *closed, 
                  QMutex         *mutex );

    /*!
     *  \fn is_finished()
     *  \brief returns true if the finish_build signal has been caught 
     */
    bool is_finished();

    /*!
     *  \fn is_killed()
     *  \brief returns true if the finish_build signal has been caught earlier
     */
    bool is_killed();
    
protected slots:
    /*!
     *  \fn run()
     *  \brief run the thread.
     */
    void run();

    /*!
     *  \fn run()
     *  \brief ends the thread.
     */
    void finish_build(bool f);
    
    /*!
     *  \fn dump(const std::string &path, const std::string &filename)
     *  \brief dumps the remaining IntervalOfContainer at path path
     */
    void dump(const std::string &path,
              const std::string &filename);
};
#endif // PARSING_THREAD_HPP

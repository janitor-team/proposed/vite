/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Clément 
**
*/
#include <fstream>
#include <list>
#include <map>
/* -- */
#include "trace/values/Values.hpp"
#include "trace/EntityTypes.hpp"
#include "trace/Entitys.hpp"
#include "trace/Trace.hpp"
/* -- */
#include "parser/Parser.hpp"
/* -- */
#include "parser/ParsingThread.hpp"

ParsingThread::ParsingThread(Parser         *p,
                             Trace          *t,
                             QWaitCondition *finished,
                             QWaitCondition *closed,
                             QMutex         *mutex)
{
    _mutex    = mutex;
    _parser   = p;
    _trace    = t;
    _finished = finished;
    _closed   = closed;
    _is_finished = false;
    _is_killed   = false;
}

void ParsingThread::run()
{
    try {
        /* Do not finish the build inside the parser,
         * we will do this after, if we don't want to
         * just dump the trace
         */
        _parser->parse(*_trace,false);
        /* set the parser's finished flag to true */
        _parser->finish();
    }
    catch (const std::string &) {
        _parser->finish();
    }

    /* Locks the mutex and automatically unlocks
     * it when going out of scope */
    QMutexLocker locker(_mutex);
    _finished->wakeAll();
}

void ParsingThread::finish_build(bool finish_trace_after_parse)
{
    //we receive an order to quit parsing to close the program
    if( !finish_trace_after_parse )
        _is_killed = true;

    //if it has been cancelled, _is_finished is already set, do not finish the trace
    if( ! _is_killed ) {
        _trace->finish();
        _parser->finish();
    }
    _is_finished = true;

    //locks the mutex and automatically unlocks it when going out of scope
    QMutexLocker locker(_mutex);
    _closed->wakeAll();
}

void ParsingThread::dump(const std::string &path,
                         const std::string &filename)
{
    _is_finished = true;

#if defined(USE_ITC) && defined(BOOST_SERIALIZE)
    if(! _is_killed)
        _trace->dump(path, filename);
#else
    (void)path;
    (void)filename;
#endif

    //locks the mutex and automatically unlocks it when going out of scope
    QMutexLocker locker(_mutex);
    _closed->wakeAll();
}

bool ParsingThread::is_finished()
{
    return _is_finished;
}

bool ParsingThread::is_killed()
{
    return _is_killed;
}


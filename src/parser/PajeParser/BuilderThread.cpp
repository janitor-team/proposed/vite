/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Clément
**
*/
#include <fstream>
#include <list>
#include <map>
#include <queue>
/* -- */
#include "trace/values/Values.hpp"
#include "trace/EntityTypes.hpp"
#include "trace/Entitys.hpp"
#include "trace/Trace.hpp"
/* -- */
#include "parser/Parser.hpp"
#include "parser/PajeParser/mt_PajeFileManager.hpp"
#include "parser/PajeParser/ParserDefinitionPaje.hpp"
#include "parser/PajeParser/mt_ParserEventPaje.hpp"
/* -- */
#include "parser/PajeParser/BuilderThread.hpp"
#include "trace/TraceBuilderThread.hpp"

BuilderThread::BuilderThread(mt_ParserEventPaje *p, Trace *trace,
                             QWaitCondition* cond,QWaitCondition* trace_cond,
                             QSemaphore *sem1, QSemaphore *sem2, QMutex *mutex,
                             QMutex *mutex2, Parser *parser) {
    _event_parser = p;
    _parser       = parser;
    _cond         = cond;
    _trace_cond   = trace_cond;
    _freeSlots    = sem1;
    _mutex        = mutex;
    _mutex2       = mutex2;
    _linesProduced= sem2;
    _trace        = trace;
    _is_finished  = false;
}

void BuilderThread::run(int n, PajeLine* line) {

    _freeSlots->acquire(); //do not produce too fast (5 blocks max at a time)

    int n_without_errors=0;
    Trace_builder_struct* tb_structs= new Trace_builder_struct[n];
    int j;
    for(j=0; j<n; j++){
        tb_structs[n_without_errors]._parser = _parser;
        if( _event_parser->store_event(&line[j],*_trace, &tb_structs[n_without_errors]) == 0)
            n_without_errors++;
        free(line[j]._tokens); // release tokens allocated in the PajeFileManager
    }
    emit(build_trace(n_without_errors, tb_structs));
    _linesProduced->release();
    free(line);
}

bool BuilderThread::is_finished() { return _is_finished; }
void BuilderThread::finish_build() {
    //quit();
    //finish the TraceBuilderThread before closing this one
    //locks the mutex and automatically unlocks it when going out of scope
    QMutexLocker locker2(_mutex2);
    emit build_finished();
    _trace_cond->wait(_mutex2);
    locker2.unlock();

    _is_finished=true;
    //locks the mutex and automatically unlocks it when going out of scope
    QMutexLocker locker(_mutex);
    _cond->wakeAll();
}

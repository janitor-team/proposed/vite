/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Clément
**
*/

#include <string>
#include <list>
#include <map>
#include <fstream>
#include <sstream>
/* -- */
#include "common/ViteConfig.hpp"
/* -- */
#include "trace/values/Values.hpp"
#include "trace/EntityTypes.hpp"
#include "trace/Entitys.hpp"
#include "trace/Trace.hpp"
/* -- */
#include "common/Message.hpp"
/* -- */
#include "parser/Parser.hpp"

#ifdef MT_PARSING
#include "parser/PajeParser/mt_ParserPaje.hpp"
#else
#include "parser/PajeParser/ParserPaje.hpp"
#endif

#include "parser/PajeParser/ParserVite.hpp"
#ifdef VITE_ENABLE_OTF
#include <otf.h>
#ifdef MT_PARSING
#include "parser/OTFParser/mt_ParserOTF.hpp"
#else
#include "parser/OTFParser/ParserOTF.hpp"
#endif
#endif //VITE_ENABLE_OTF

#ifdef WITH_OTF2
#include <otf2/otf2.h>
#include "parser/OTF2Parser/ParserOTF2.hpp"
#endif //WITH_OTF2

#ifdef VITE_ENABLE_TAU
#include <TAU_tf.h>
#include "parser/TauParser/ParserTau.hpp"
#endif //VITE_ENABLE_TAU

#ifdef BOOST_SERIALIZE
#include "ParserSplitted.hpp"
#endif

#include "ParserFactory.hpp"

using namespace std;

bool ParserFactory::create(Parser **parser,
                           const string &filename)
{
    size_t pos = filename.find_last_of('.');
    string ext = "";

    if ( pos != string::npos ) {
        ext = filename.substr(pos);

        if( ext == ".trace" ) {
            /* WARNING: Why MT parsing has a different name ? it should just replace the other ones */
#ifdef MT_PARSING
            *parser = new mt_ParserPaje(filename);
#else
            *parser = new ParserPaje(filename);
#endif
        }
        else if( ext == ".ept" ) {
            *parser = new ParserVite(filename);
        }
        else if( ext == ".otf" ) {
#ifdef VITE_ENABLE_OTF
#ifdef MT_PARSING
            *parser = new mt_ParserOTF(filename);
#else
            *parser = new ParserOTF(filename);
#endif
#else
            *Message::get_instance() << "OTF parser was not compiled. Use parser Paje by default" << Message::endw;
            return false;
#endif //VITE_ENABLE_OTF
        }
        else if( ext == ".otf2" ) {
#ifdef WITH_OTF2
            *parser = new ParserOTF2(filename);
#else
            *Message::get_instance() << "OTF2 parser was not compiled. Use parser Paje by default" << Message::endw;
            return false;
#endif //WITH_OTF2
        }
        else if( (ext == ".trc") || (ext == ".edf") ) {
#ifdef VITE_ENABLE_TAU
            *parser = new ParserTau(filename);
#else
            *Message::get_instance() << "Tau parser was not compiled. Use parser Paje by default" << Message::endw;
            return false;
#endif //VITE_ENABLE_TAU
        }
        else if(ext == ".vite") {
#ifdef BOOST_SERIALIZE
            *parser = new ParserSplitted(filename);
#else
            *Message::get_instance() << "Boost serialization was not compiled. We cannot parse .vite files" << Message::endw;
#endif //VITE_ENABLE_OTF
        }
    }

    if( *parser == nullptr ) {
#ifdef MT_PARSING
        *parser = new mt_ParserPaje(filename);
#else
        *parser = new ParserPaje(filename);
#endif
    }

    return true;
}

/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Clément
**
*/
#ifndef STATE_HPP
#define STATE_HPP

/*!
 * \file State.hpp
 */
/*!
 * \class State
 * \brief Describe the state of a container
 */
class State: public Entity {
private:
    Date _start, _end;
    StateType *_type;
    EntityValue *_value;

public:
    /*!
     * \brief Constructor
     */
    State(Date start, Date end, StateType *type, Container *container, EntityValue *value, std::map<std::string, Value *> opt);
    State();
    /*!
     * \fn get_start_time() const;
     * \brief Get the start time of the state
     */
    Date get_start_time() const;

    /*!
     * \fn get_end_time() const
     * \brief Get the end time of the state
     */
    Date get_end_time() const;

    /*!
     * \fn get_duration() const
     * \brief Get the duration of the state
     */
    double get_duration() const;

    /*!
     * \fn get_type() const
     * \brief Get the type of the state
     */
    const StateType *get_type() const;

    /*!
     * \fn get_value() const
     * \brief Get the value of the state
     * \return Pointer to the Entityvalue or NULL if it has no value
     */
    //const EntityValue *get_value() const;

    EntityValue *get_value() const;

    /*!
     * \fn set_left_date(Date)
     * \brief To set the left date
     */
    void set_left_date(Date);

};

#endif

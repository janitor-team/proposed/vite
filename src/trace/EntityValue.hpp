/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Clément
**
*/
#ifndef ENTITYVALUE_HPP
#define ENTITYVALUE_HPP
#include <QObject>

#include <map>
#include "common/common.hpp"
#include "trace/values/Values.hpp"

/*!
 * \file EntityValue.hpp
 */
class EntityType;

/*!
 * \class EntityValue
 * \brief Describe the value of an entity
 */
class EntityValue : public QObject {
    Q_OBJECT

private:
    Name        _name;
    EntityType *_type;
    std::map<std::string, Value *> _opt;

    Color *_filecolor;
    Color *_usedcolor;
    bool   _visible;

public:
    /*!
     * \fn EntityValue(const Name &name, EntityType *type, std::map<std::string, Value *> opt);
     * \brief EntityValue Constructor
     * \param name Name of the value
     * \param type EntityType to which the value apply
     * \param opt Extra fields
     */
    EntityValue(const Name &name, EntityType *type, std::map<std::string, Value *> opt);
    ~EntityValue() override;

    /*!
     * \fn get_name() const
     * \brief Get the name of the value
     */
    Name get_Name() const;

    std::string get_name()  const;
    std::string get_alias() const;

    /*!
     * \fn get_type() const
     * \brief Get the entity type of the value
     */
    const EntityType *get_type() const;

    /*!
     * \fn get_extra_fields() const
     * \brief Get the extra fields
     */
    const std::map<std::string, Value *> *get_extra_fields() const;

    Color *get_used_color() const;
    Color *get_file_color() const;
    bool   get_visible() const;
    EntityClass_t get_class() const;

    void set_used_color(Color *c);
    void set_visible(bool b);

    void reload_file_color();

signals:
    void changedColor( EntityValue * );
    void changedVisible( EntityValue * );
};

#endif

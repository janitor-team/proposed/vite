#include <string>
#include <map>
#include <iostream>
/* -- */
#include "trace/values/Value.hpp"
#include "trace/values/Color.hpp"
/* -- */
#include "common/Session.hpp"
/* -- */
#include "Palette.hpp"

Palette::Palette(const std::string &name) : _name(name) {
}

Palette::~Palette() {
    for(auto & _pair : _pairs) {
        Color *c = _pair.second;
        delete c;
    }
}


Color *Palette::get_color(const std::string &state_name) {
    if(_pairs.find(state_name) != _pairs.end())
        return _pairs[state_name];
    else{
        return nullptr;
    }
}

bool Palette::is_visible(const std::string &state_name){
    if (_visible.count(state_name) > 0)
        return _visible[state_name];
    else
        return true;
}

void Palette::add_state(const std::string &state, Color &c, bool visible) {
    if(_pairs.find(state) != _pairs.end()) {
        Color *tmp = _pairs[state];
        delete tmp;
    }
    _pairs[state] = new Color(c);
    _visible[state] = visible;
}

void Palette::clear(){
    _pairs.clear();
}

std::string Palette::get_name() const {
    return _name;
}

std::map<std::string, Color *> Palette::get_map() const {
    return _pairs;
}

/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Clément
**
*/
#ifndef MESSAGE_HPP
#define MESSAGE_HPP

/*!
 * \class Message
 * \brief Contains routine for easily send messages of informations, warnings or errors to the user
 */

class stringstream;
class ostream;
class Interface;
#include <sstream>

class Message: public std::stringstream {
private:
    static Message   *_message;
    static Interface *_interface;

    Message();

public:
    static const class end_error_t { } ende;
    static const class end_warning_t { } endw;
    static const class end_information_t { } endi;
    static const class end_selection_information_t { } endsi;

    static Message *get_instance();
    static void     kill();

    static void       set_interface(Interface *);
    static Interface *get_interface();

};

std::ostream &operator<<(std::ostream &, Message::end_error_t);
std::ostream &operator<<(std::ostream &, Message::end_warning_t);
std::ostream &operator<<(std::ostream &, Message::end_information_t);
std::ostream &operator<<(std::ostream &, Message::end_selection_information_t);

#endif

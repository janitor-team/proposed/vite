/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Clément
**
*/
/*!
 *\file common.hpp
 *\brief This file gives some global defines and typedef.
 */

#ifndef COMMON_HPP
#define COMMON_HPP

#include "ViteConfig.hpp"

/*!
 * \def VITE_ORGANISATION_NAME
 * \brief the ViTE string
 */
#define VITE_ORGANISATION_NAME "ViTE"

/*!
 * \def VITE_ORGANISATION_DOMAIN
 * \brief the ViTE string
 */
#define VITE_ORGANISATION_DOMAIN "vite.gforge.inria.fr"

/*!
 * \def VITE_APPLICATION_NAME
 * \brief the ViTE string
 */
#define VITE_APPLICATION_NAME "vite"

/*!
 * \def VITE_VERSION
 * \brief the ViTE version (string)
 */
//#define VITE_VERSION "1.2"
/*!
 * \def VITE_DATE
 * \brief the ViTE release date (string)
 */
//#define VITE_DATE "July 2010"
/*!
 * \def VITE_WEBSITE
 * \brief the ViTE website (string)
 */
#define VITE_WEBSITE "http://vite.gforge.inria.fr"

/*!
 * \brief Unity for count elements such as number of states or events... NOT USED YET
 */
typedef long Element_count;

/*!
 * \brief Unity for variables of position of a element in the display such as height, abscissa...
 */
typedef float Element_pos;

/*!
 * \brief Unity for colors
 */
typedef float Element_col;

/*!
 * \brief Define the unity of time.
 */
typedef double Times;

/*!
 * \def M_PI
 * \brief PI if not defined (mscv for example)
 */
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

/* TODO : move this somewhere else when we will have the message handler */
#define vite_warning(str) std::cerr << str << std::endl;
#define vite_error(str)   std::cerr << str << std::endl;

#endif /* COMMON_H */

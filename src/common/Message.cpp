/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Clément
**
*/

#include <iostream>
#include <sstream>
/* -- */
#include "common.hpp"
/* -- */


#ifdef VITE_DEBUG
/* Test mode */
class Interface {
public:
    void error(const std::string& s) {
        std::cerr <<"ERROR: " << s << std::endl;
    }
    void warning(const std::string& s) {
        std::cerr <<"WARNING: " << s << std::endl;
    }
    void information(const std::string& s) {
        std::cerr <<"INFORMATION: " << s << std::endl;
    }

    void selection_information(const std::string& s) {
        std::cerr <<"INFORMATION: " << s << std::endl;
    }

    std::string get_filename(){
        return "filename ";
    }
};
#else
/* Release mode */
#include "interface/Interface.hpp"
#endif
/* -- */
#include "common/Message.hpp"

#ifdef WIN32
#undef interface // Stupid windows :)
#endif

Message   *Message::_message   = nullptr;
Interface *Message::_interface = nullptr;

#ifdef _MSC_VER // TODO : Check if needed for icc
const Message::end_error_t Message::ende;
const Message::end_warning_t Message::endw;
const Message::end_information_t Message::endi;
const Message::end_selection_information_t Message::endsi;
#endif

Message::Message() = default;

Message *Message::get_instance() {
    if (_message)
        return _message;
    else
        return _message = new Message();
}

void Message::kill() {
    if (_message)
        delete _message;
    _message = nullptr;
}

void Message::set_interface(Interface *interface) {
    _interface = interface;
}

Interface *Message::get_interface() {
    return _interface;
}

std::ostream &operator<<(std::ostream &out, Message::end_error_t) {
    Message *message = Message::get_instance();
    Interface *interface = Message::get_interface();

    if (interface) {
        interface->error(message->str());
        message->str(""); // flush the stream
    } else {
      vite_error("Warning: no interface designed to display messages.");
    }
    return out;
}

std::ostream &operator<<(std::ostream &out, Message::end_warning_t) {
    Message *message = Message::get_instance();
    Interface *interface = Message::get_interface();

    if (interface) {
        interface->warning(message->str());
        message->str(""); // flush the stream
    } else {
      vite_error("Warning: no interface designed to display messages.");
    }
    return out;
}

std::ostream &operator<<(std::ostream &out, Message::end_information_t) {
    Message *message = Message::get_instance();
    Interface *interface = Message::get_interface();

    if (interface) {
        interface->information(message->str());
        message->str(""); // flush the stream
    } else {
        vite_error("Warning: no interface designed to display messages.");
    }
    return out;
}


std::ostream &operator<<(std::ostream &out, Message::end_selection_information_t) {
    Message *message = Message::get_instance();
    Interface *interface = Message::get_interface();

    if (interface) {
        interface->selection_information(message->str());
        message->str(""); // flush the stream
    } else {
      vite_error("Warning: no interface designed to display messages.");
    }
    return out;
}

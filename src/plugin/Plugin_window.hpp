/*
** This file is part of the ViTE project.
**
** This software is governed by the CeCILL-A license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
**
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
**
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-A license and that you accept its terms.
**
**
** ViTE developers are (for version 0.* to 1.0):
**
**        - COULOMB Kevin
**        - FAVERGE Mathieu
**        - JAZEIX Johnny
**        - LAGRASSE Olivier
**        - MARCOUEILLE Jule
**        - NOISETTE Pascal
**        - REDONDY Arthur
**        - VUCHENER Clément
**
*/

#ifndef PLUGINWINDOW_HPP
#define PLUGINWINDOW_HPP

#include <QObject>
#include <QMainWindow>

class Plugin;
class QPushButton;
class QTabWidget;
class QVBoxLayout;
class Core;

/*!
 * \class Plugin_window
 * \brief The window which shows all the plugins.
 */
class Plugin_window : public QMainWindow {
    Q_OBJECT
private:
    QVBoxLayout *_layout;
    QTabWidget *_tab_widget;
    QPushButton *_execute_button;
    std::vector<Plugin *> _plugins;
    int _current_index;
    Core *_core;

    QStringList _plugin_directories;

public:
    /*!
     * \fn Plugin_window(Core *c, QWidget *parent = NULL)
     * \brief Constructor
     * \param c the Core
     * \param parent the parent widget
     */
    Plugin_window(Core *c, QWidget *parent = nullptr);

    /*!
     * \fn ~Plugin_window()
     * \brief Destructor
     */
    ~Plugin_window() override;

    /*!
     * \fn load_list()
     * \brief Don't remember what this should do... maybe totally useless
     * maybe store the names of the plugins... Yep totally useless
     * \todo remove this useless function
     */
    void load_list();

    /*!
     * \fn load_plugin(const std::string &plugin_name)
     * \brief Load the static plugin.
     * \param plugin_name the name of the plugin we want to load
     */
    void load_plugin(const std::string &plugin_name);

    /*!
     * \fn update_trace()
     * \brief Modify the trace of all the plugins.
     */
    void update_trace();

    /*!
     * \fn show()
     * \brief Show the window. Overriden to initialise the current plugin first.
     */
    void show();

    /*!
     * \fn clear_plugins()
     * \brief Call the clear method of all the loaded plugins.
     */
    void clear_plugins();

    /*!
     * \fn reload_plugins()
     * \brief Remove all the plugin and reload all the plugins.
     */
    void reload_plugins();
private:
    /*!
     * \fn load_shared_plugins()
     * \brief Load all the shared plugins that can be found in the directories set in the Settings module.
     */
    void load_shared_plugins();
     /*!
     * \fn add_plugin(Plugin *plug)
     * \brief Add a plugin to the list and to the window.
     * \param plug the plugin to add.
     */
   void add_plugin(Plugin *plug);

public slots:
    /*!
     * \fn reload_plugin(int index)
     * \brief Reload the trace of the index-th plugin
     * \param index the index of the plugin
     */
    void reload_plugin(int index);
    /*!
     * \fn execute_plugin()
     * \brief Execute the current plugin
     */
    void execute_plugin();
};


#endif // PLUGINWINDOW_HPP

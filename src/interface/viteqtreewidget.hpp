#ifndef VITETREEWIDGET_H
#define VITETREEWIDGET_H
 
#include <QTreeWidget>
#include <QObject>
 
class viteQTreeWidget : public QTreeWidget
{
    Q_OBJECT
 
public:
    viteQTreeWidget(QWidget *parent = nullptr);
    ~viteQTreeWidget() override;
protected:
    void dropEvent(QDropEvent *event) override;
    
    void dragEnterEvent(QDragEnterEvent *e) override;
void dragMoveEvent(QDragMoveEvent *e) override;

QList<QTreeWidgetItem *> selected_items;

QMimeData *mimeData(const QList<QTreeWidgetItem *> items) const override;

bool dropMimeData ( QTreeWidgetItem * newParentPtr, int index, const QMimeData * data, Qt::DropAction action ) override;

Qt::DropActions supportedDropActions () const override;
};

#endif

#
# CPack configuration to create packages
#
set(CPACK_PACKAGE_VERSION_MAJOR ${VITE_VERSION_MAJOR})
set(CPACK_PACKAGE_VERSION_MINOR ${VITE_VERSION_MINOR})
set(CPACK_PACKAGE_VERSION_PATCH ${VITE_VERSION_PATCH})
set(CPACK_SOURCE_GENERATOR "TBZ2")
set(CPACK_SOURCE_PACKAGE_FILE_NAME
  "${CMAKE_PROJECT_NAME}-${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH}")

if(WIN32)
  set(CPACK_NSIS_EXECUTABLES_DIRECTORY "bin")
  set(CPACK_PACKAGE_EXECUTABLES "vite" "ViTE")

  install(FILES ${GLEW_DLL} DESTINATION bin)
  install(FILES ${ZLIB_DLL} DESTINATION bin)
  # install Qt dlls
  set(QT_DLL_PATH ${Qt5Core_DIR}/../../../bin)
  get_target_property(QtCoreDll Qt5::Core "IMPORTED_LOCATION_RELEASE")
  get_target_property(QtGuiDll Qt5::Gui "IMPORTED_LOCATION_RELEASE")
  get_target_property(QtOpenGLDll Qt5::OpenGL "IMPORTED_LOCATION_RELEASE")
  get_target_property(QtWidgetsDll Qt5::Widgets "IMPORTED_LOCATION_RELEASE")
  get_target_property(QtXmlDll Qt5::Xml "IMPORTED_LOCATION_RELEASE")
	
  install(FILES ${QtCoreDll} ${QtGuiDll} ${QtOpenGLDll} ${QtWidgetsDll} ${QtXmlDll}
    DESTINATION bin)
  
  install(FILES ${QT_DLL_PATH}/../plugins/platforms/qwindows.dll
    DESTINATION bin/platforms)
	
  install(FILES ${QT_DLL_PATH}/libgcc_s_dw2-1.dll ${QT_DLL_PATH}/libwinpthread-1.dll ${QT_DLL_PATH}/libstdc++-6.dll ${QT_DLL_PATH}/icudt54.dll ${QT_DLL_PATH}/icuin54.dll ${QT_DLL_PATH}/icuuc54.dll 
	DESTINATION bin)
endif(WIN32)

set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "ViTE is a trace explorer. It is a tool to visualize execution traces in Pajé or OTF format for debugging and profiling parallel or distributed applications.")
set(CPACK_RESOURCE_FILE_LICENSE "${PROJECT_SOURCE_DIR}/licence/Licence_CeCILL_V2-en.txt")
set(CPACK_RESOURCE_FILE_README "${PROJECT_SOURCE_DIR}/README")

set(CPACK_SOURCE_IGNORE_FILES
  /\\\\.svn/
  ~$
  CMakeCache\\\\.txt
  cmake_dist\\\\.cmake
  CPackSourceConfig\\\\.cmake
  CPackConfig.cmake
  /cmake_install\\\\.cmake
  /CTestTestfile\\\\.cmake
  /CMakeFiles/
  /_CPack_Packages/
  $\\\\.gz
  $\\\\.zip
  /CMakeFiles/
  /_CPack_Packages/
  Makefile$
  ${CPACK_SOURCE_IGNORE_FILES}
)

include(CPack)

add_custom_target(dist COMMAND ${CMAKE_MAKE_PROGRAM} package_source)


Source: vite
Section: devel
Priority: optional
Maintainer: Samuel Thibault <sthibault@debian.org>
Build-Depends: debhelper-compat (= 12),
 cmake,
 qttools5-dev,
 libqt5opengl5-desktop-dev, libqt5charts5-dev, libqt5svg5-dev,
 libglm-dev, libglew-dev,
 texlive-latex-base, texlive-latex-recommended, texlive-latex-extra,
 libotf-trace-dev,
 texlive-fonts-recommended,
 ghostscript, zlib1g-dev
Rules-Requires-Root: no
Standards-Version: 4.5.0
Homepage: http://vite.gforge.inria.fr/
Vcs-Browser: https://salsa.debian.org/debian/vite
Vcs-Git: https://salsa.debian.org/debian/vite.git

Package: vite
Architecture: any
Multi-Arch: foreign
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Efficient visual trace explorer
 ViTE is a powerful portable and open source profiling tool to visualize
 the behaviour of parallel applications. Thanks to its scalable design,
 ViTE helps programmers to efficiently analyze the performance of
 potentially large applications.
 .
 ViTE currently enables the visualisation of traces using the Pajé format
 (open format, see http://www-id.imag.fr/Logiciels/paje/) and has various
 functionalities such as exporting a view in a svg format to integrate
 the results easily in a report, or viewing statistics.
